# Check for AD DS Domain Configuration, set up if not present

function Get-TargetResource
{
    [OutputType([System.Collections.Hashtable])]
    param
    (
        [Parameter(Mandatory)]
        [String]$DomainName,

        [String]$ParentDomainName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdministratorPassword,

        [PSCredential]$DnsDelegationCredential
    )

    $returnValue = @{
        Name = $DomainName
        Ensure = $false
    }

    try
    {
        $fullDomainName = $DomainName
        if( $ParentDomainName )
        {
          $fullDomainName = $DomainName + "." + $ParentDomainName
        }

        Write-Verbose -Message "Resolving $fullDomainName..."
        $domain = Get-ADDomain -Identity $fullDomainName -Credential $DomainAdministratorCredential
        if( $domain -ne $null )
        {
            Write-Verbose -Message "Domain $fullDomainName is present. Looking for DCs"
            try
            {
                $dc = Get-ADDomainController -Identity $env:COMPUTERNAME -Credential $DomainAdministratorCredential
                Write-Verbose -Message "Got Domain Controller $($dc.Name) in domain $($dc.Domain). Parent domain was $($dc.ParentDomain), $ParentDomainName was asked for"
                if(($dc.Domain -eq $DomainName) -and ( ( !($dc.ParentDomain) -and  !($ParentDomainName) ) -or ($dc.ParentDomain -eq $ParentDomainName)))
                {
                    Write-Verbose -Message "Current node $($dc.Name) is already a domain controller for $($dc.Domain). Parent Domain "
                    $returnValue.Ensure = $true
                }
            }
            catch
            {
                Write-Verbose -Message "The local computer does not host a domain controller"
            }
        }
    }
    catch
    {
        Write-Verbose -Message "Target Machine is not running AD WS, and hence is not a domain controller"
    }
    $returnValue
}


function Set-TargetResource
{
    param
    (
        [Parameter(Mandatory)]
        [String]$DomainName,

        [String]$ParentDomainName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdministratorPassword,

        [PSCredential]$DnsDelegationCredential
    )
    
    $parameters = $PSBoundParameters.Remove("Debug");
    $state = Test-TargetResource @PSBoundParameters
    if( $state -eq $true )
    {
        Write-Verbose -Message "Already at desired state. Nothing to set."
        return
    }

    $fullDomainName = $DomainName
    if( $ParentDomainName )
    {
      $fullDomainName = $DomainName + "." + $ParentDomainName
    }
    
    Write-Verbose -Message "Checking if Domain $fullDomainName is present ..."
    # Check if the domain exists
    $domain = $null;
    try
    {
        $domain = Get-ADDomain -Identity $fullDomainName -Credential $DomainAdministratorCredential
    }
    catch
    {
    }
    if( $domain -ne $null )
    {
        Write-Error -Message "Domain $DomainName is already present, but is not hosted by this node. Returning error"
        throw (new-object -TypeName System.InvalidOperationException -ArgumentList "Domain $Name is already present, but is not hosted by this node")
    }

    Write-Verbose -Message "Verified that Domain $DomainName is not already present in the network. Going on to create the domain."
    if( ( $ParentDomainName -eq $null ) -or ( $ParentDomainName -eq "" ) )
    {
        Write-Verbose -Message "Domain $DomainName is NOT present. Creating Forest $DomainName ..."
    
        $params = @{ DomainName = $DomainName; SafeModeAdministratorPassword = $SafemodeAdministratorPassword.Password; NoRebootOnCompletion = $true; InstallDns = $true; Force = $true }
        if( $DnsDelegationCredential -ne $null )
        {
            $params.Add( "DnsDelegationCredential", $DnsDelegationCredential )
            $params.Add( "CreateDnsDelegation", $true )
        }
        Install-ADDSForest @params 
                    
        Write-Verbose -Message "Created Forest $DomainName"
    }
    else
    {
        Write-Verbose -Message "Domain $DomainName is NOT present. Creating domain $DomainName as a child of $ParentDomainName..."
        Import-Module -Name ADDSDeployment
        $params = @{ NewDomainName = $DomainName; ParentDomainName = $ParentDomainName; DomainType = [Microsoft.DirectoryServices.Deployment.Types.DomainType]::ChildDomain; SafeModeAdministratorPassword = $SafemodeAdministratorPassword.Password; Credential = $DomainAdministratorCredential; NoRebootOnCompletion = $true; InstallDns = $true; Force = $true }
        if( $DnsDelegationCredential -ne $null )
        {
            $params.Add( "DnsDelegationCredential", $DnsDelegationCredential )
            $params.Add( "CreateDnsDelegation", $true )
        }
        Install-ADDSDomain @params        
        Write-Verbose -Message "Created Domain $DomainName"
    }
    
    Write-Verbose -Message "Indicating to LCM that system needs reboot."
    $global:DSCMachineStatus = 1 
}


function Test-TargetResource
{
    [OutputType([System.Boolean])]
    param
    (
        [Parameter(Mandatory)]
        [String]$DomainName,

        [String]$ParentDomainName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdministratorPassword,

        [PSCredential]$DnsDelegationCredential
    )
    try
    {
        $parameters = $PSBoundParameters.Remove("Debug");
        $existingResource = Get-TargetResource @PSBoundParameters
        $existingResource.Ensure
    }
    # If the domain doesn't exist
    catch
    {
        Write-Verbose -Message "Domain $Name is NOT present on the node"
        $false
    } 
}

# Check that target machine(s) have been promoted properly as AD DS Domain Controller 

function Get-TargetResource
{
    [OutputType([System.Collections.Hashtable])]
    param
    (
        [Parameter(Mandatory)]
        [String]$DomainName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdministratorPassword
    )

    $returnValue = @{
        Name = $DomainName
        Ensure = $false
    }

    try
    {
        Write-Verbose -Message "Resolving $DomainName..."
        $domain = Get-ADDomain -Identity $DomainName -Credential $DomainAdministratorCredential
        if( $domain -ne $null )
        {
            Write-Verbose -Message "Domain $DomainName is present. Looking for DCs"
            try
            {
                $dc = Get-ADDomainController -Identity $env:COMPUTERNAME -Credential $DomainAdministratorCredential
                Write-Verbose -Message "Got Domain Controller $($dc.Name) in domain $($dc.Domain)."
                if($dc.Domain -eq $DomainName)
                {
                    Write-Verbose -Message "Current node $($dc.Name) is already a domain controller for $($dc.Domain)."
                    $returnValue.Ensure = $true
                }
            }
            catch
            {
                Write-Verbose -Message "No domain controllers could be contacted for $DomainName"
            }
        }
    }
    catch
    {
        Write-Error -Message "Target Machine is not running AD WS, and hence is not a domain controller"
        throw $_
    }
    $returnValue
}


function Set-TargetResource
{
    param
    (
        [Parameter(Mandatory)]
        [String]$DomainName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdministratorPassword
    )
    
    $parameters = $PSBoundParameters.Remove("Debug");
    $state = Test-TargetResource @PSBoundParameters
    if( $state -eq $true )
    {
        Write-Verbose -Message "Already at desired state. Returning."
        return
    }

    Write-Verbose -Message "Checking if Domain $DomainName is present ..."
    # Check if the domain exists
    $domain = $null;
    try
    {
        $domain = Get-ADDomain -Identity $DomainName -Credential $DomainAdministratorCredential
    }
    catch
    {
        Write-Error -Message "Domain $DomainName could not be found. Assert a domain resource first."
        throw (new-object -TypeName System.InvalidOperationException -ArgumentList "Domain $DomainName could not be found.")
    }

    Write-Verbose -Message "Verified that Domain $DomainName is present in the network. Going on to create the domain controller."

    Install-ADDSDomainController -DomainName $DomainName -Force -Verbose:$false -NoRebootOnCompletion `
                        -SafeModeAdministratorPassword $SafemodeAdministratorPassword.Password `
                        -Credential $DomainAdministratorCredential
                
    Write-Verbose -Message "Node is now a domain controller for $DomainName"
    Write-Verbose -Message "Indicating to LCM that system needs reboot."
    $global:DSCMachineStatus = 1 

}


function Test-TargetResource
{
    [OutputType([System.Boolean])]
    param
    (
        [Parameter(Mandatory)]
        [String]$DomainName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [Parameter(Mandatory)]
        [PSCredential]$SafemodeAdministratorPassword
    )

    try
    {
        $parameters = $PSBoundParameters.Remove("Debug");
        $existingResource = Get-TargetResource @PSBoundParameters
        $existingResource.Ensure
    }
    # If the domain doesn't exist
    catch
    {
        Write-Error -Message "Domain $DomainName is NOT present on the node"
        throw $_
    } 
}

# Now validate a user

function Get-TargetResource
{
    param
    (
        [Parameter(Mandatory)]
        [string]$DomainName,

        [Parameter(Mandatory)]
        [string]$UserName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,
        
        [PSCredential]$Password,

        [ValidateSet("Present","Absent")]
        [string]$Ensure = "Present"                   
    )

    try
    {
        Write-Verbose -Message "Checking if the user $UserName in domain $DomainName is present ..."
        $user = Get-AdUser -Identity $UserName -Credential $DomainAdministratorCredential
        Write-Verbose -Message "User $UserName in domain $DomainName is present."
        $Ensure = "Present"
    }
    # User not found
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        Write-Verbose -Message "User $UserName account in domain $DomainName is NOT present"
        $Ensure = "Absent"
    }
    catch
    {
        Write-Error -Message "Unhandled exception looking up $UserName account in domain $DomainName."
        throw $_
    }

    @{
        DomainName = $DomainName
        UserName = $UserName
        Ensure = $Ensure
    }
}

function Set-TargetResource
{
    param
    (
        [Parameter(Mandatory)]
        [string]$DomainName,

        [Parameter(Mandatory)]
        [string]$UserName,
        
        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [PSCredential]$Password,

        [ValidateSet("Present","Absent")]
        [string]$Ensure = "Present"                    
    )
    try
    {
        ValidateProperties @PSBoundParameters -Apply
    }
    catch
    {
        Write-Error -Message "Error setting AD User $UserName in domain $DomainName. $_"
        throw $_
    }
}

function Test-TargetResource
{
    param
    (
        [Parameter(Mandatory)]
        [string]$DomainName,

        [Parameter(Mandatory)]
        [string]$UserName,
        
        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [PSCredential]$Password,

        [ValidateSet("Present","Absent")]
        [string]$Ensure = "Present"          
    )

    try
    {
        $parameters = $PSBoundParameters.Remove("Debug");
        ValidateProperties @PSBoundParameters    
    }
    catch
    {
        Write-Error -Message "Error testing AD User $UserName in domain $DomainName. $_"
        throw $_
    }
}

function ValidateProperties
{
    param
    (
        [Parameter(Mandatory)]
        [string]$DomainName,

        [Parameter(Mandatory)]
        [string]$UserName,

        [Parameter(Mandatory)]
        [PSCredential]$DomainAdministratorCredential,

        [PSCredential]$Password,

        [ValidateSet("Present","Absent")]
        [string]$Ensure = "Present",          

        [Switch]$Apply
    )

    $result = $true
    # Check if user exists and if user exists validate the password
    try
    {
        Write-Verbose -Message "Checking if the user $UserName in domain $DomainName is present ..."
        $user = Get-AdUser -Identity $UserName -Credential $DomainAdministratorCredential
        Write-Verbose -Message "User $UserName in domain $DomainName is present."
        
        if( $Ensure -eq "Absent" )
        {
            if( $Apply )
            {
                Remove-ADUser -Identity $UserName -Credential $DomainAdministratorCredential -Confirm:$false
                return
            }
            else
            {
                return $false
            }
        }
        
        if($Apply)
        {
            # If account is not enabled, enable it. Needed for password validation
            If(!($user.Enabled))
            {
                Set-AdUser -Identity $UserName -Enabled $true -Credential $DomainAdministratorCredential
                Write-Verbose -Message "Enabled $UserName account in domain $DomainName."
            }
        }
        
        # If password is specified, check if it is valid
        if($Password)
        {
            Write-Verbose -Message "Checking if the user $UserName password is valid ..."
            Add-Type -AssemblyName 'System.DirectoryServices.AccountManagement'
            
            Write-Verbose -Message "Creating connection to the domain $DomainName ..."
            $prnContext = new-object System.DirectoryServices.AccountManagement.PrincipalContext(
                            "Domain", $DomainName, $DomainAdministratorCredential.UserName, `
                            $DomainAdministratorCredential.GetNetworkCredential().Password)

            # This can return true or false
            $result = $prnContext.ValidateCredentials($UserName,$Password.GetNetworkCredential().Password)
            if($result)
            {
                Write-Verbose -Message "User $UserName password is valid"
                return $true
            }
            else
            {
                Write-Verbose -Message "User $UserName password is NOT valid"
                if($Apply)
                {
                    Set-AdAccountPassword -Reset -Identity $UserName -NewPassword $Password.Password -Credential $DomainAdministratorCredential
                    Write-Verbose -Message "User $UserName password has been reset"
                }
                else
                {
                    return $false
                }
            }
        }
        else
        {
            Write-Verbose -Message "User $UserName account in domain $DomainName is present"
            return $true
        }
    }
    # User not found
    catch [Microsoft.ActiveDirectory.Management.ADIdentityNotFoundException]
    {
        Write-Verbose -Message "User $UserName account in domain $DomainName is NOT present"
        if($Apply)
        {
            if( $Ensure -ne "Absent" )
            {
                $params = @{ Name = $UserName; Enabled = $true; Credential = $DomainAdministratorCredential }
                if( $Password )
                {
                    $params.Add( "AccountPassword", $Password.Password )
                }
                New-AdUser @params
                Write-Verbose -Message "User $UserName account in domain $DomainName has been created"
            }
        }
        else
        {
            return ( $Ensure -eq "Absent" )
        }
    }
}

Export-ModuleMember -Function *-TargetResource

# Now that AD DS should be configured, enforce some basic settings such as parameterized TCP/IP settings

configuration Sample_xIPAddress_Parameterized
{
    param
    (

        [string[]]$NodeName = 'localhost',

        [Parameter(Mandatory)]
        [string]$IPAddress,

        [Parameter(Mandatory)]
        [string]$InterfaceAlias,

        [Parameter(Mandatory)]
        [string]$DefaultGateway,

        [int]$SubnetMask = 16,

        [ValidateSet("IPv4","IPv6")]
        [string]$AddressFamily = 'IPv4'
    )

    Import-DscResource -Module xNetworking

    Node $NodeName
    {
        xIPAddress NewIPAddress
        {
            IPAddress      = $IPAddress
            InterfaceAlias = $InterfaceAlias
            DefaultGateway = $DefaultGateway
            SubnetMask     = $SubnetMask
            AddressFamily  = $AddressFamily
        }
    }
}

# Check for optional Windows Features

# This PS module contains functions for Desired State Configuration Windows Optional Feature provider. It enables configuring optional features on Windows Client SKUs.

# Fallback message strings in en-US
DATA localizedData
{
    # culture = "en-US"
    ConvertFrom-StringData @'                
        DismNotAvailable = PowerShell module Dism could not be imported.
        NotAClientSku = This Resource is only available for Windows Client.
        ElevationRequired = This Resource requires to be run as an Administrator.
        ValidatingPrerequisites = Validating prerequisites...
        CouldNotCovertFeatureState = Could not convert feature state '{0}' into Enable/Disable.
        EnsureNotSupported = The value '{0}' for property Ensure is not supported.
        RestartNeeded = Target machine needs to be restarted.
        GetTargetResourceStartMessage = Begin executing Get functionality on the {0} feature.
        GetTargetResourceEndMessage = End executing Get functionality on the {0} feature.
        SetTargetResourceStartMessage = Begin executing Set functionality on the {0} feature.
        SetTargetResourceEndMessage = End executing Set functionality on the {0} feature.
        TestTargetResourceStartMessage = Begin executing Test functionality on the {0} feature.
        TestTargetResourceEndMessage = End executing Test functionality on the {0} feature.
        FeatureInstalled = Installed feature {0}.
        FeatureUninstalled = Uninstalled feature {0}.
'@
}
Import-Module Dism -Force -ErrorAction SilentlyContinue

function Get-TargetResource
{
    [CmdletBinding()]
    [OutputType([System.Collections.Hashtable])]
    param
    (
        [parameter(Mandatory = $true)]
        [System.String]
        $Name
    )

    Write-Debug ($LocalizedData.GetTargetResourceStartMessage -f $Name)

    ValidatePrerequisites

    $result = Dism\Get-WindowsOptionalFeature -FeatureName $Name -Online

    $returnValue = @{
        LogPath = $result.LogPath
        Ensure = ConvertStateToEnsure $result.State
        CustomProperties = SerializeCustomProperties $result.CustomProperties
        Name = $result.FeatureName
        LogLevel = $result.LogLevel
        Description = $result.Description
        DisplayName = $result.DisplayName
    }

    $returnValue

    Write-Debug ($LocalizedData.GetTargetResourceEndMessage -f $Name)
}

# Serializes a list of CustomProperty objects into [System.String[]]
function SerializeCustomProperties
{
    param
    (
        $CustomProperties
    )

    $CustomProperties | ? {$_ -ne $null} | % { "Name = $($_.Name), Value = $($_.Value), Path = $($_.Path)" }
}

# Converts state returned by Dism Get-WindowsOptionalFeature cmdlet to Enable/Disable
function ConvertStateToEnsure
{
    param
    (
        $State
    )

    if ($state -eq 'Disabled')
    {
        'Disable'
    }
    elseif ($state -eq 'Enabled')
    {
        'Enable'
    }
    else
    {
        Write-Warning ($LocalizedData.CouldNotCovertFeatureState -f $state)
        $state
    }
}


function Set-TargetResource
{
    [CmdletBinding()]
    param
    (
        [System.String[]]
        $Source,

        [System.Boolean]
        $RemoveFilesOnDisable,

        [System.String]
        $LogPath,

        [parameter(Mandatory = $true)]
        [ValidateSet("Enable","Disable")]
        [System.String]
        $Ensure,

        [System.Boolean]
        $NoWindowsUpdateCheck,

        [parameter(Mandatory = $true)]
        [System.String]
        $Name,

        [ValidateSet("ErrorsOnly","ErrorsAndWarning","ErrorsAndWarningAndInformation")]
        [System.String]
        $LogLevel
    )

    Write-Debug ($LocalizedData.SetTargetResourceStartMessage -f $Name)

    ValidatePrerequisites

    switch ($LogLevel)
    {
        'ErrorsOnly' { $DismLogLevel = 'Errors' }
        'ErrorsAndWarning' { $DismLogLevel = 'Warnings' }
        'ErrorsAndWarningAndInformation' { $DismLogLevel = 'WarningsInfo' }
        '' { $DismLogLevel = 'WarningsInfo' }
    }

    # construct parameters for Dism cmdlets
    $PSBoundParameters.Remove('Name') > $null
    $PSBoundParameters.Remove('Ensure') > $null
    if ($PSBoundParameters.ContainsKey('RemoveFilesOnDisable'))
    {
        $PSBoundParameters.Remove('RemoveFilesOnDisable')
    }

    if ($PSBoundParameters.ContainsKey('NoWindowsUpdateCheck'))
    {
        $PSBoundParameters.Remove('NoWindowsUpdateCheck')
    }

    if ($PSBoundParameters.ContainsKey('LogLevel'))
    {
        $PSBoundParameters.Remove('LogLevel')
    }
    
    if ($Ensure -eq 'Enable')
    {
        if ($NoWindowsUpdateCheck)
        {
            $feature = Dism\Enable-WindowsOptionalFeature -FeatureName $Name -Online -LogLevel $DismLogLevel @PSBoundParameters -LimitAccess
        }
        else
        {
            $feature = Dism\Enable-WindowsOptionalFeature -FeatureName $Name -Online -LogLevel $DismLogLevel @PSBoundParameters
        }

        Write-Verbose ($LocalizedData.FeatureInstalled -f $Name)
    }
    elseif ($Ensure -eq 'Disable')
    {
        if ($RemoveFilesOnDisable)
        {
            $feature = Dism\Disable-WindowsOptionalFeature -FeatureName $Name -Online -LogLevel $DismLogLevel @PSBoundParameters -Remove
        }
        else
        {
            $feature = Dism\Disable-WindowsOptionalFeature -FeatureName $Name -Online -LogLevel $DismLogLevel @PSBoundParameters
        }

        Write-Verbose ($LocalizedData.FeatureUninstalled -f $Name)
    }
    else
    {
        throw ($LocalizedData.EnsureNotSupported -f $Ensure)
    }

    if ($feature.RestartNeeded)
    {
        Write-Verbose $LocalizedData.RestartNeeded
        $global:DSCMachineStatus = 1
    }

    Write-Debug ($LocalizedData.SetTargetResourceEndMessage -f $Name)
}


function Test-TargetResource
{
    [CmdletBinding()]
    [OutputType([System.Boolean])]
    param
    (
        [System.String[]]
        $Source,

        [System.Boolean]
        $RemoveFilesOnDisable,

        [System.String]
        $LogPath,

        [ValidateSet("Enable","Disable")]
        [System.String]
        $Ensure,

        [System.Boolean]
        $NoWindowsUpdateCheck,

        [parameter(Mandatory = $true)]
        [System.String]
        $Name,

        [ValidateSet("ErrorsOnly","ErrorsAndWarning","ErrorsAndWarningAndInformation")]
        [System.String]
        $LogLevel
    )

    Write-Debug ($LocalizedData.TestTargetResourceStartMessage -f $Name)

    ValidatePrerequisites

    $result = Dism\Get-WindowsOptionalFeature -FeatureName $Name -Online

    if ($result -eq $null)
    {
        $result = 'Disabled'
    }

    if (($result.State -eq 'Disabled' -and $Ensure -eq 'Disable')`
        -or ($result.State -eq 'Enabled' -and $Ensure -eq 'Enable'))
    {
        $true
    }
    else
    {
        $false
    }

    Write-Debug ($LocalizedData.TestTargetResourceEndMessage -f $Name)
}

# ValidatePrerequisites is a helper function used to validate if the MSFT_WindowsOptionalFeature is supported on the target machine.
function ValidatePrerequisites   
{
    Write-Verbose $LocalizedData.ValidatingPrerequisites

    # check that we're running on a client SKU
    $os = Get-CimInstance -ClassName  Win32_OperatingSystem
    
    if ($os.ProductType -ne 1)
    {
        throw $LocalizedData.NotAClientSku
    }

    # check that we are running elevated
    $windowsIdentity = [System.Security.Principal.WindowsIdentity]::GetCurrent()
    $windowsPrincipal = new-object System.Security.Principal.WindowsPrincipal($windowsIdentity)
    $adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator

    if (!$windowsPrincipal.IsInRole($adminRole))
    {
        throw $LocalizedData.ElevationRequired
    }

    # check that Dism PowerShell module is available
    Import-Module Dism -Force -ErrorVariable ev -ErrorAction SilentlyContinue

    if ($ev.Count -gt 0)
    {
        throw $LocalizedData.DismNotAvailable
    }
}

Export-ModuleMember -Function *-TargetResource
