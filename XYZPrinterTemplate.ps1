﻿<#
.Synopsis
   This cmdlet uses DSC to check computers at company XYZ based on a number of factors, and ensures that the proper printer make and model are installed and defaulted.
.DESCRIPTION
   Long description
.EXAMPLE
   Set-XYZ-Printers -Group "Finance" -ACode $True -ForceDef $True
.EXAMPLE
  Set-XYZ-Printers -Upgrade -B64 -B32
.EXAMPLE
  Set-XYZ-Printers -Upgrade -Group "HR"
.INPUTS
   $NTopology
.OUTPUTS
   $PrintState
.NOTES
   This tool is licensed by agreement. (C) 2014 by Damien Stanton. All Rights Reserved
.COMPONENT
   This cmdlet is a part of a larger automation module to be named.
.FUNCTIONALITY
   This cmdlet automates the installation, management and reporting of network-enabled multifunction printers, scanners and copiers
#>
function Set-XYZPrinters
{
    [CmdletBinding(DefaultParameterSetName='Default Params',
                  SupportsShouldProcess=$true,
                  PositionalBinding=$false,
                  HelpUri = '//PATH', #TODO
                  ConfirmImpact='High')]
    [OutputType([Int])]
    Param
    (
        # Param1 help description
        [Parameter(Mandatory=$true,
                   ValueFromPipeline=$true,
                   ValueFromPipelineByPropertyName=$true,
                   ValueFromRemainingArguments=$false,
                   Position=0,
                   ParameterSetName='Group')]
        [ValidateNotNull()]
        [ValidateNotNullOrEmpty()]
        [ValidateCount(0,5)]
        [ValidateSet('Finance', 'HR', 'Operations', 'IT')]
        [Alias("p1")]
        $Param1,

        # Param2 help description
        [Parameter(ParameterSetName='Upgrade')]
        [AllowNull()]
        [AllowEmptyCollection()]
        [AllowEmptyString()]
        [ValidateScript({$true})]
        [ValidateRange(0,5)]
        [int]
        $Param2,

        # Param3 help description
        [Parameter(ParameterSetName='HR')]
        [ValidatePattern("[a-z]*")]
        [ValidateLength(0,15)]
        [String]
        $Param3
    )

    Begin
    {
        #TODO
		function locatePrinter
		{
				
		}
    }
    Process
    {
        if ($pscmdlet.ShouldProcess("Target", "Operation"))
        {
        #TODO
        }
    }
    End
    {
        #CLEANUP TASKS, Check for proper return val
    }
}
