#PowerShell
---

A bucket for PowerShell stuff.

Current Items:

+  XYZTemplate is a simple module that allows an array of network printers to be deployed via one call to the cmdlet.
+  "Custom_DSC" is an example of a DSC resource built upon the official (experimental) DSC Resource Kit Wave 5 released to MSDN.

`
λD
`
